#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sim_parameters import use_fitting

if not use_fitting:
    import numpy as ar
    array = ar.array
else:
    import torch as ar
    array = ar.tensor

import numpy as np
import torch as tr
    
import scipy.special as scs
import matplotlib.pylab as plt
import seaborn as sns
import multiprocessing.pool as mpp
import json
import pickle
import jsonpickle as pickle
import jsonpickle.ext.numpy as jsonpickle_numpy

def istarmap(self, func, iterable, chunksize=1):
    """starmap-version of imap
    """
    self._check_running()
    if chunksize < 1:
        raise ValueError(
            "Chunksize must be 1+, not {0:n}".format(
                chunksize))

    task_batches = mpp.Pool._get_tasks(func, iterable, chunksize)
    result = mpp.IMapIterator(self)
    self._taskqueue.put(
        (
            self._guarded_task_generation(result._job,
                                          mpp.starmapstar,
                                          task_batches),
            result._set_length
        ))
    return (item for chunk in result for item in chunk)

mpp.Pool.istarmap = istarmap

def save_file(obj,fname):

    jsonpickle_numpy.register_handlers()
    pickled = pickle.encode(obj)
    with open(fname, 'w') as outfile:
        json.dump(pickled, outfile)


def load_file(fname):

    jsonpickle_numpy.register_handlers()
    with open(fname, 'r') as infile:
        data = json.load(infile)
    obj = pickle.decode(data)   
    return obj


class Dict2Class(object):
      
    def __init__(self, my_dict):
          
        for key in my_dict:
            setattr(self, key, my_dict[key])


def convert(perception):
    new_perception = {}
    dict_perception = perception.__dict__
    for key in dict_perception:
        entry = dict_perception[key]
        is_tensor = tr.is_tensor(entry)
        is_list = isinstance(entry, list)
        # print(type(entry),is_tensor, is_list)
        if is_list:
            new_perception[key] = [e.numpy() for e in entry]
        if is_tensor:
            new_perception[key] = entry.numpy()
        if not is_tensor and not is_list:
            new_perception[key] = entry
    
    new_perception = Dict2Class(new_perception)
    return new_perception
        # print(type(new_perception[key]),is_tensor, is_list)


def own_logical_and(x,y):
    z = x*y
    return z>0


def own_logical_or(x,y):
    z = x+y
    return z>0


def ln(x,debugging=True):

    #with ar.errstate(divide='ignore'):

    if debugging:
        if isinstance(x, tr.Tensor):
            return tr.log(x+1e-20)#ar.nan_to_num(ar.log(x))
        else:
            return np.log(x+1e-20)#ar.nan_to_num(ar.log(x))
    else:
        return ar.log(x+1e-20)#ar.nan_to_num(ar.log(x))


def logit(x):
    with ar.errstate(divide = 'ignore'):
        return ar.nan_to_num(ar.log(x/(1-x)))


def logistic(x):
    return 1/(1+ar.exp(-x))


def softmax(x,debugging=True):
    """Compute softmax values for each sets of scores in x."""

    if debugging:
        if isinstance(x, tr.Tensor):
            e_x = tr.exp(x - tr.max(x, axis = 0)[0])
        else:
            e_x = np.exp(x - np.max(x, axis = 0))
    else:
        if not use_fitting:
            e_x = ar.exp(x - ar.max(x, axis = 0))
        else:
            e_x = ar.exp(x - ar.max(x, axis = 0)[0])
            
    return e_x / e_x.sum(axis = 0)


def sigmoid(x, a=1., b=1., c=0., d=0.):
    f = a/(1. + ar.exp(-b*(x-c))) + d
    return f


def exponential(x, b=1., c=0., d=0.):
    f = ar.exp(b*(x-c)) + d
    return f


def lognormal(x, mu, sigma):
    return -.5*(x-mu)*(x-mu)/(2*sigma) - .5*ln(2*ar.pi*sigma)


def lognormal3(x, mu, sigma, c):
    return 1./((x-a)*sigma*ar.sqrt(2*ar.pi)) * exp(-(ln(x-a)-mu)**2/(2*sigma**2))


def Beta_function(a):
    return scs.gamma(a).prod()/scs.gamma(a.sum())


def logBeta(a):
    return scs.loggamma(a).sum() - scs.loggamma(a.sum())


def D_KL_nd_dirichlet(alpha, beta):
    D_KL = 0
    assert(len(alpha.shape) == 3)
    for j in range(alpha.shape[1]):
        D_KL += -scs.gammaln(alpha[:,j]).sum(axis=0) + scs.gammaln(alpha[:,j].sum(axis=0)) \
         +scs.gammaln(beta[:,j]).sum(axis=0) - scs.gammaln(beta[:,j].sum(axis=0)) \
         + ((alpha[:,j]-beta[:,j]) * (scs.digamma(alpha[:,j]) - scs.digamma(alpha[:,j].sum(axis=0))[ar.newaxis,:])).sum(axis=0)

    return D_KL


def D_KL_dirichlet_categorical(alpha, beta):

    D_KL = -scs.gammaln(alpha).sum(axis=0) + scs.gammaln(alpha.sum(axis=0)) \
     +scs.gammaln(beta).sum(axis=0) - scs.gammaln(beta.sum(axis=0)) \

    for k in range(alpha.shape[1]):
        helper = ar.zeros(alpha.shape[1])
        helper[k] = 1
        D_KL += alpha[k]/alpha.sum(axis=0)*((alpha-beta) * (scs.digamma(alpha) -\
                     scs.digamma((alpha+helper).sum(axis=0))[ar.newaxis,:])).sum(axis=0)

    return D_KL


def plot_habit_learning(w, results, save_figs=False, fname=''):

    #plot Rho
#    plt.figure(figsize=(10,5))
    arm_cols = ['royalblue','blue']
#    for i in range(1,w.agent.nh):
#        plt.plot(w.environment.Rho[:,i,i], label="arm "+str(i), c=arm_cols[i-1], linewidth=3)
#    plt.ylim([-0.1,1.1])
#    plt.legend(fontsize=16, bbox_to_anchor=(1.04,1), loc="upper left", ncol=1) #bbox_to_anchor=(0, 1.02, 1, 0.2), mode="expand"
#    plt.yticks(fontsize=18)
#    plt.xticks(fontsize=18)
#    plt.xlabel("trial", fontsize=20)
#    plt.ylabel("reward probabilities", fontsize=20)
#    #plt.title("Reward probabilities for each state/bandid")
#    if save_figs:
#        plt.savefig(fname+"_Rho.svg")
#        plt.savefig(fname+"_Rho.png", bbox_inches = 'tight', dpi=300)
#    plt.show()
#
#    plt.figure()
#    sns.barplot(data=results.T, ci=95)
#    plt.xticks([0,1],["won", "chosen", "context"])
#    plt.ylim([0,1])
#    #plt.title("Reward rate and rate of staying with choice with habit")
#    plt.yticks(fontsize=18)
#    plt.xticks(fontsize=18)
#    plt.xlabel("trial", fontsize=20)
#    plt.ylabel("rates", fontsize=20)
#    if False:
#        plt.savefig(fname+"_habit.svg")
#    plt.show()

    plt.figure(figsize=(10,5))
    for i in range(1,w.agent.nh):
        plt.plot(w.environment.Rho[:,i,i], label="arm "+str(i), c=arm_cols[i-1], linewidth=3)
    for t in range(1,w.agent.T):
        plt.plot(w.agent.posterior_context[:,t,1], ".", label="context", color='deeppink')
    plt.ylim([-0.1,1.1])
    plt.legend(fontsize=16, bbox_to_anchor=(1.04,1), loc="upper left", ncol=1) #bbox_to_anchor=(0, 1.02, 1, 0.2), mode="expand"
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    plt.xlabel("trial", fontsize=20)
    plt.ylabel("reward probabilities", fontsize=20)
    ax = plt.gca().twinx()
    ax.set_ylim([-0.1,1.1])
    ax.set_yticks([0,1])
    ax.set_yticklabels(["$c_{1}$","$c_{2}$"],fontsize=18)
    ax.yaxis.set_ticks_position('right')
    #plt.title("Reward probabilities and context inference")
    if save_figs:
        plt.savefig(fname+"_Rho_c_nohabit.svg")
        plt.savefig(fname+"_Rho_c_nohabit.png", bbox_inches = 'tight', dpi=300)
    plt.show()

    plt.figure(figsize=(10,5))
    for i in range(1,w.agent.nh):
        plt.plot(w.environment.Rho[:,i,i], label="arm "+str(i), c=arm_cols[i-1], linewidth=3)
    for t in range(w.agent.T-1):
        plt.plot((w.actions[:,t]-1), ".", label="action", color='darkorange')
    plt.ylim([-0.1,1.1])
    plt.legend(fontsize=16, bbox_to_anchor=(1.04,1), loc="upper left", ncol=1) #bbox_to_anchor=(0, 1.02, 1, 0.2), mode="expand"
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    plt.xlabel("trial", fontsize=20)
    plt.ylabel("reward probabilities", fontsize=20)
    ax = plt.gca().twinx()
    ax.set_ylim([-0.1,1.1])
    ax.set_yticks([0,1])
    ax.set_yticklabels(["$a_{1}$","$a_{2}$"],fontsize=18)
    ax.yaxis.set_ticks_position('right')
    #plt.title("Reward probabilities and chosen actions")
    if save_figs:
        plt.savefig(fname+"_Rho_a_nohabit.svg")
        plt.savefig(fname+"_Rho_a_nohabit.png", bbox_inches = 'tight', dpi=300)
    plt.show()

    plt.figure(figsize=(10,5))
    for i in range(1,w.agent.nh):
        plt.plot(w.environment.Rho[:,i,i], label="arm "+str(i), c=arm_cols[i-1], linewidth=3)
    for t in range(w.agent.T-1):
        plt.plot((w.agent.posterior_policies[:,t,2]* w.agent.posterior_context[:,t]).sum(axis=1), ".", label="action", color='darkorange')
    plt.ylim([-0.1,1.1])
    plt.legend(fontsize=16, bbox_to_anchor=(1.04,1), loc="upper left", ncol=1) #bbox_to_anchor=(0, 1.02, 1, 0.2), mode="expand"
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    plt.xlabel("trial", fontsize=20)
    plt.ylabel("reward probabilities", fontsize=20)
    ax = plt.gca().twinx()
    ax.set_ylim([-0.1,1.1])
    ax.set_yticks([0,1])
    ax.set_yticklabels(["$a_{1}$","$a_{2}$"],fontsize=18)
    ax.yaxis.set_ticks_position('right')
    #plt.title("Reward probabilities and chosen actions")
    if save_figs:
        plt.savefig(fname+"_Rho_a_nohabit.svg")
        plt.savefig(fname+"_Rho_a_nohabit.png", bbox_inches = 'tight', dpi=300)
    plt.show()

#    plt.figure(figsize=(10,5))
#    for i in range(1,w.agent.nh):
#        plt.plot(w.environment.Rho[:,i,i]*w.agent.perception.prior_rewards[i], label="arm "+str(i), c=arm_cols[i-1], linewidth=3)
#    for t in range(w.agent.T-1):
#        plt.plot((w.actions[:,t]-1), ".", label="action", color='g', alpha=0.5)
#    plt.ylim([-0.1,1.1])
#    plt.legend(fontsize=16, bbox_to_anchor=(1.04,1), loc="upper left", ncol=1) #bbox_to_anchor=(0, 1.02, 1, 0.2), mode="expand"
#    plt.yticks(fontsize=18)
#    plt.xticks(fontsize=18)
#    plt.xlabel("trial", fontsize=20)
#    plt.ylabel("reward probabilities", fontsize=20)
#    #plt.title("Expected utility and chosen actions")
#    if False:
#        plt.savefig(fname+"_utility_a_habit.svg")
#        plt.savefig(fname+"_utility_a_habit.png", bbox_inches = 'tight', dpi=300)
#    plt.show()
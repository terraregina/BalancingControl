"""
Central simulation file, where the simulation parameters for all simulations that one wants to run are specified.
Imported into run_example_habit.py and run_example_habit_group
"""

import numpy as np

use_fitting = False                 # use torch agent or not
deterministic_action = False        # always sample argmax of probability over actions p(a)
deterministic_reward = False        # always sample argmax of probability over rewards p(r|s)
print_thoughts = False              # prints simulation output while simulation running
pooled = False                      # whether to paralelize simulations or not
repetitions = 5                     # how many instances of an agent to run

infer_h = False                     # infer habitual tendency
infer_dec = False                   # infer decision temperature
init_reward_count_bias = 10         # how much of the true reward distribution the agent knows 
h = [1,100]#, 2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90] # habitual tendency
cue_ambiguity = [0.8]               # parameter used to initialize p(context_observation|context); the higher the value the higher 
context_trans_prob = [0.85]
cue_switch = [False]
reward_naive = [False]
training_blocks = [4]
degradation_blocks=[4]
degradation = [True]
trials_per_block=[70]
dec_temps = [1]
rews = [0]
rewards = [[-1,0,1]]#[[-1,1]]
conf = ['blocked']

na = 2                                           # number of unique possible actions
nc = 4                                           # number of contexts, planning and habit
nr = len(rewards[0])
ns = 6                                           # number of unique travel locations
npl = nr
no = 2
steps = 3                                        # numbe of decisions made in an episode
T = steps + 1                                    # episode length
matrix = 'new'


if npl == 3:
    planet_reward_probs = np.array([[0.95, 0   , 0   ],
                                    [0.05, 0.95, 0.05],
                                    [0,    0.05, 0.95]]).T
    planet_reward_probs_switched = np.array([[0   , 0    , 0.95],
                                            [0.05, 0.95 , 0.05],
                                            [0.95, 0.05 , 0.0]]).T

    # planet_reward_probs = np.array([[0.9, 0   , 0   ],
    #                                 [0.1, 0.9, 0.1],
    #                                 [0,    0.1, 0.9]]).T 
    # planet_reward_probs_switched = np.array([[0   , 0    , 0.9],
    #                                         [0.1, 0.9 , 0.1],
    #                                         [0.9, 0.1 , 0.0]]).T

elif npl == 2:
    planet_reward_probs = np.array([[0.95,   0.05   ],
                                    [0.05,   0.95]]) 

    planet_reward_probs_switched = np.array([[0.05,   0.95   ],
                                                [0.95,   0.05]])
                                                    

if matrix == 'old':

    state_transition_matrix = np.zeros([ns,ns,na])
    m = [1,2,3,4,5,0]
    for r, row in enumerate(state_transition_matrix[:,:,0]):
        row[m[r]] = 1

    j = np.array([5,4,5,6,2,2])-1

    for r, row in enumerate(state_transition_matrix[:,:,1]):
        row[j[r]] = 1   
    state_transition_matrix = np.transpose(state_transition_matrix, axes= (1,0,2))
    state_transition_matrix = np.repeat(state_transition_matrix[:,:,:,np.newaxis], repeats=nc, axis=3)

elif matrix == 'new':

    state_transition_matrix = np.zeros([ns,ns,na])
    m = [1,2,3,4,5,0]
    for r, row in enumerate(state_transition_matrix[:,:,0]):
        row[m[r]] = 1
        
    j = [2,3,4,5,0,1]

    for r, row in enumerate(state_transition_matrix[:,:,1]):
        row[j[r]] = 1
    state_transition_matrix = np.transpose(state_transition_matrix, axes= (1,0,2))
    state_transition_matrix = np.repeat(state_transition_matrix[:,:,:,np.newaxis], repeats=nc, axis=3)


nc = 4
utility = [[1, 9 , 90]] if nr == 3 else [[1,99]]
arr_type = 'torch' if use_fitting else 'numpy'





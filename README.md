# BalancingControl

Prior-based Bayesian habit learning model

GOAL: Work myself back into the code and organize it with sarah's structure

# File role and content description

## Trial Generation
- **create_trials** - original file that creates the trial compilation;
- create_config_files_planning -> all_possible_trials -> generate trials df >
- **create_trials_experiment.py** - a copy of this file that was hopefully used to generate experimental dataframe; it was only used once, the history of the commit can be seen at: https://github.com/SIHranova/BalancingControl/commits/working/create_trials_experiment.py 
"
# Things done
- created a "old" folder to store files i have alrady explored and decided i do not need
- familirize myself with sarah's folder structure and recreate it
  
### create_trials.py
- Accidentally started working on create_trials_experiment.py >_>; 
- Renamed a lot of functions to make more sense and simplified the function that calculates expectations; I compared its output (for only one contingency with the old version of the code); things seem the same;
- switched over to create_trials and simplified call signature and rewrote a lot of it

### run_example_habit.py
- Can be used to run simulations defined in sim_parameters.py with single agent fitting
    - non-parallelized simulation works;
    - check that both parallelized and non-parallelized calls work;
 
### multiple_plot.py 
- can be used to plot masters figure showing behavioural optimality, context inference and reward contingencies
- 
### run_example_habit_group.py
- Can be used to run simulations defined in sim_parameters.py with Group agent fitting

### action_selection.py
- removed mention of deterministic rewards; not sure if function works correctly with fitting agent; needs to be renamed. 



# Things to think about
- trials are generated such that there is a reward gaining bias
- there appears to be a high policy bias in the planning condition as well? for policy 4
- i must have just changed the plotting to checking if context inferred correctly across all four time points; I have changed it back and things look better but now for some reason the rewards DKL is not changing? Which is strange..

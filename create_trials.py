#%% 

#########  IMPORTS  ################

import numpy as np
from itertools import product
import json  as js
import pandas as pd
import os

############ FUNCTION DEFINITIONS ###############

def create_state_transition_matrix(ns, na, targets):
    ''' 
    creates state transition matrix p( s_t | s_t-1 , a )
    ns = number of possibles states, na = number of possible actions
    targets = p( s_t| s_t-1 = j, a = i) 
    '''
    assert na == targets.shape[0], "length of target planets should match number of possible planets"

    state_transition_matrix = np.zeros([ns,ns,na])

    for action in range(na):
        target = targets[action]
        for r, row in enumerate(state_transition_matrix[:,:,action]):
            row[target[r]] = 1

    return state_transition_matrix

def sequence_of_length_n(r,n):
    ''' 
    creates all permutations of a vector r with length n and k unique digits
    for r = [0,1] and length of sequence n = 3
    000 001 010 011 100 101 etc 
    '''
    r = [r]*n
    return np.asarray(list(product(*r)))


def calculate_trial_policy_expectation(conf, exp_reward, stm, policy):
    '''
    function calculating the expected trial reward for all possible
    starting positions, given a planet constelation and policy.

    conf = planet configuration
    exp_reward = expected reward of planet
    stm = state transition matrix
    policy = action policy such as jump jump move
    '''

    exp_reward = exp_reward[conf]   # extract reward probabilities for planets
    exp_reward = np.repeat(exp_reward.reshape(1, exp_reward.shape[0]), repeats = stm.shape[0], axis=0)
    
    # path holds position of rocket after each action; different rows correspond to different starting positions    
    path = np.zeros([stm.shape[0], stm.shape[1], 3])

    path[:,:,0] = stm[:,:,policy[0]]
    for action in range(len(policy)-1):
        path[:,:,action+1] = path[:,:,action].dot(stm[:,:,policy[action+1]])

    expectation = np.zeros([stm.shape[0], stm.shape[1]])
    for action in range(3):
        expectation += path[:,:,action]*exp_reward            

    expectation = expectation.sum(axis=1)

    return expectation


def create_all_possible_trials(expected_rewards):
    """
    This function creates all possible trials for a given planet-reward mapping,
    number of planets in a configuration and number of actions and time points.
    
    It also removes all trial configurations that have more than one best optimal policy

    @Parameters:
        - expected_rewards: the expected reward of each planet type
    @Output:
        - slices: a list of length equal to the total number of policies. Each list entry
            holds a dataframe with all the trials where that policy is optimal
        - planet_confs: a numpy array that holds all possible trial conformations
    """
    ns = n_planet_positions

    if nr == 3:
        planet_confs = sequence_of_length_n([0,1,2],ns)
    elif nr==2:
        planet_confs = sequence_of_length_n([0,1],ns)

    planet_confs = planet_confs[1:-1]                                        # generate all planet constelations and remove first and last constellation
    expectations = np.zeros([ns, pol_ind.shape[0], planet_confs.shape[0]])   # array holding trial expected reward for every possible trial, starting position and policy
    
    for ci, conf in enumerate(planet_confs):
        for m, policy in enumerate(policies):
            expectations[:, m, ci] = calculate_trial_policy_expectation(conf, expected_rewards, state_transition_matrix, policy)
    
    col_names = ['conf_ind', 'planet_conf_ind', 'planet_conf', 'start', 'policy', 'expected_reward']
    
    s = 0                                                          # unique trial configuration index
    rr = 0                                                         # row index
    nrows = planet_confs.shape[0]*ns*policies.shape[0]             # rows the dataframe will have
    data_array = np.empty([nrows, len(col_names)],dtype=object)    # initialized as object in order to be able to add planet conf as array

    for ci, conf in enumerate(planet_confs):
        for st in np.arange(ns):
            for m, policy in enumerate(policies):
                data_array[rr,:] = [s, ci, (conf), st, m, expectations[st,m,ci]]
                rr += 1
            s += 1

    data = pd.DataFrame(data_array, columns = col_names)
    datatype = {'conf_ind':int, 'start':int, 'planet_conf_ind':int, 'policy':int, 'expected_reward':float}
    data = data.astype(datatype)

    data['max_reward'] = data.groupby('conf_ind')[['expected_reward']].transform('max')   # calculate max reward from all possible policies for a given trial
    data = data.round(3)                                                                  # round the result so that == operation gives correct result
    data['optimal'] = data['max_reward'] == data['expected_reward']                       # define optimal policies
    data['total_optimal'] = data.groupby(['conf_ind'])[['optimal']].transform('sum')      # count optimal policies
    data = data.drop(data[data.total_optimal != 1].index)                                 # drop all configurations that have more than 1 optimal policy

    slices = [None for pi in range(len(policies))]
    for pi in pol_ind:
        slice = data.loc[( data['optimal'] == True) & ( data['policy'] == pi)]
        slices[pi] = slice

    return slices


def create_trials_for_both_contingencies(extend=False, seed = 1):

    '''
    Creates "data" which holds all trial information for a given experiment
    Data is a list of of length number of policies. Each entry holds a list of matrices with all trials where a given policy
    is optimal under a given contingency. If extended is set to true, each matrix holds multiple copies of its trials. 
    This is necessarry when having an experiment with many trials, where all possible trials are used atleast once. 
    
    The rows of the matrix hold info for a single trial and the columns the trial information:
        which policy is optimal in this trial - column 1
        starting position of rocket - column 2
        planet constellation - columns 3 to 8
        expected reward for trial if optimal policy chosen - column 9
    @Parameters:
        extended: whether to copy created trials 5 times in data. Set to True if running out of trials in "create_trials_planning" 
    @Output:
       data
    '''
    
    np.random.seed(seed)
    ns = n_planet_positions

    slices = [None for nn in range(n_reward_contingencies)]          # array that holds dataframes with all trials for all contingencies
    for ri in range(n_reward_contingencies):
        slices[ri] = create_all_possible_trials(contingency_exp_rewards[ri])


    data = [[] for i in range(len(pol_ind))]                         # array that holds trial information, separated in terms of which policy is optimal 

    for pol in pol_ind:
        for ci, trials_for_given_contingency in enumerate(slices):

            # create trial matrix dt
            slice = trials_for_given_contingency[pol]                # extact all trials where pol is optimal
            ntrials = slice.shape[0]                                 # count how many there are
            dt = np.zeros([slice.shape[0], 3 + ns])

            # populate trial matrix dt
            plnts = np.array(slice.planet_conf.to_list())            # planets constellations where policy pol is optimal
            strts = slice.start.to_list()                            # starting points where policy pol is optimal
            expected_reward = slice.expected_reward.to_list()        # expected reward for that trial
            
            dt[:,0] = [pol]*ntrials                                  # optimal sequence index
            dt[:,1] = strts                                          # trial starting position
            dt[:,2:-1] = plnts                                       # planets
            dt[:,-1] = expected_reward                               # expected_reward
            np.random.shuffle(dt)                                    # shuffle trials
            
            # this is rewritten and untested!
            if extend:
                dt = np.vstack([dt for i in range(5)])
            data[pol].append(dt)

    return data


def create_trials_planning(data,                                         # all possible trials where a given policy is optimal
                           habit_pol = 3,                                # which policy is being habituated
                           contingency_degradation = True,               # will there be contingency switch
                           switch_cues= False,                           # will context cue mapping switch
                           training_blocks = 2,                          # how many training blocks (with original reward contingency)
                           degradation_blocks = 1,                       # how many degradation blocks (with switched reward contingency)
                           extinction_blocks = 2,                        # how many extinction blocks
                           trials_per_block = 28,                        # how many trials per experimental block
                           export = True,                                # save config file or not
                           blocked=False,                                # whether context presentation order is blocked
                           block = None,                                 # context block size
                           shuffle=False,                                # whether context presentation is shuffled? potentially wrong.
                           seed=1): 

    np.random.seed(seed)

    assert trials_per_block % 2 == 0, 'Please set an even number of trials per block!'

    nblocks = training_blocks + degradation_blocks + extinction_blocks     # how many blocks of trials in experiment
    half_block = trials_per_block//2                                       # how many trials of each context (habit vs planing)
    ntrials = nblocks*trials_per_block//2                                  # how many trials PER CONTEXT
    ncols = data[0][0].shape[1]                                            # how many columns in final config matrix "trials"

    trials = np.zeros([nblocks*trials_per_block, ncols])                   # final config matrix which holds all experimental data
    trial_type = np.zeros(nblocks*trials_per_block)                        # whether training, degradation or extinction
    context = np.zeros(nblocks*trials_per_block)                           # whether planning or habit context trial
    blocks = np.zeros(nblocks*trials_per_block)                            # which (experimental) block is this trial in
    
    if block is not None:
        miniblocks = half_block//block                                     # how many context blocks (called miniblocks) are in an experimental trial block


    # split data for each contingency and policy into list for all possible expected rewards
    # this way the expected reward between habit and planning trials can be matched

    unique_exp_rew = np.unique(data[0][0][:,-1])                                        # all possible expected rewards for optimal policy that are shared by all policies
    probs = (np.arange(unique_exp_rew.size)+1)/(np.arange(unique_exp_rew.size)+1).sum() # weight probablity of drawing trial by rewards it can give
    rewards = np.random.choice(np.arange(unique_exp_rew.size), p=probs, size=ntrials)   # draw expected reward for half of the trials, to ensure that the earned points between cotnexts are matched
    planning_seqs = pol_ind[pol_ind != habit_pol]
    planning_seqs = np.tile(planning_seqs, half_block // planning_seqs.size)

    split_data = [[None for contingency in range(2)] for policy in pol_ind]            # list holding data aranged in terms of optimal policy, contingency and exp_reward

    for policy in range(len(data)):
        for contingency in range(len(data[policy])):

            dat = data[policy][contingency]
            split_data[policy][contingency] = [dat[dat[:,-1]==k] for k in unique_exp_rew]
            for k in range(unique_exp_rew.size):
                np.random.shuffle(split_data[policy][contingency][k])


    """
    Populate trial matrix
    THIS ALL STILL NEEDS TO BE DOUBLE CHECKED!
    """
    for i in range(nblocks):                                                    # iterate over all blocks (training, degradation and extinction)

        # set trial type (tt) indicating experimental phases and active contingency
        if i < training_blocks:
            tt = 0
            contingency = 0
        elif i >= training_blocks and i < training_blocks + degradation_blocks:
            tt = 1                                                         
            contingency = 1                                                    
        else:
            contingency = 0
            tt = 2

        if not blocked:                                                                   # if context presentation NOT grouped
            
            # populate habit seq trials
            for t in range(half_block):                                                   # half-block perspective index (0:half-black)
                tr = int(i*trials_per_block + t)                                          # whole experiment persepctive index (0:total number of trials)
                ri = half_block*i + t                                                     # possible trial perspective index (pulls out elements from split_data)
                trials[tr,:] = split_data[habit_pol][contingency][rewards[ri]][0]
                split_data[habit_pol][contingency][rewards[ri]] = \
                    np.roll(split_data[habit_pol][contingency][rewards[ri]], -1, axis=0)

            # populate planning trials
            for t in range(half_block):
                tr = int(i*trials_per_block + half_block + t)
                ri = half_block*i + t
                trials[tr,:] = split_data[planning_seqs[t]][contingency][rewards[ri]][0]
                split_data[planning_seqs[t]][contingency][rewards[ri]] = \
                    np.roll(split_data[planning_seqs[t]][contingency][rewards[ri]], -1, axis=0)

            #shuffle trials
            np.random.shuffle(trials[i*trials_per_block:(i+1)*trials_per_block,:])

        if blocked:                                                                        # if context presentation blocked


            # pick planing and habit trials with same expected reward
            habit_trials = np.zeros([half_block, trials.shape[1]])
            planning_trials = np.zeros([half_block, trials.shape[1]])
            
            trial_shift = i*trials_per_block + half_block
            reward_shift = half_block*i

            for t in range(half_block):
                tr = trial_shift + t
                ri = reward_shift + t

                habit_trials[t,:] = split_data[habit_pol][contingency][rewards[ri]][0]
                split_data[habit_pol][contingency][rewards[ri]] = \
                    np.roll(split_data[habit_pol][contingency][rewards[ri]], -1, axis=0)
            

                planning_trials[t,:] = split_data[planning_seqs[t]][contingency][rewards[ri]][0]
                split_data[planning_seqs[t]][contingency][rewards[ri]] = \
                    np.roll(split_data[planning_seqs[t]][contingency][rewards[ri]], -1, axis=0)
                

            # shuffle planning trials so that they do not have the same order of optimal sequence within and between blocks
            np.random.shuffle(planning_trials)

            # populate block with grouped context presentation (habit first and then context)
            for mb in range(miniblocks):
                tr = int(i*trials_per_block)
                trials[tr+2*mb*block:tr+(2*mb+1)*block] = habit_trials[mb*block:(mb+1)*block,:]
                trials[tr+(2*mb+1)*block:tr+(2*mb+2)*block] = planning_trials[mb*block:(mb+1)*block,:]
            

        trial_type[i*trials_per_block:(i+1)*trials_per_block] = int(tt)
        blocks[i*trials_per_block:(i+1)*trials_per_block] = int(i)
        context[i*trials_per_block:(i+1)*trials_per_block] = trials[i*trials_per_block:(i+1)*trials_per_block,0] != habit_pol


    # create data frame and save file
    trial_type = trial_type.astype('int32')
    fname = 'planning_config_'+'degradation_'+ str(int(contingency_degradation))+ '_switch_' + str(int(switch_cues))\
             + '_train' + str(training_blocks) + '_degr' + str(degradation_blocks) + '_n' + str(trials_per_block)\
             + '_nr_' + str(nr) +'.json'

    if blocked:
        path = os.path.join(os.getcwd(), os.path.join('config','blocked'))
    elif not blocked:
        path =os.path.join(os.getcwd(), os.path.join('config','not_blocked'))

    print(fname)
    fname = os.path.join(path, fname)


    if export:
        config = {
                  'context' : context.astype('int32').tolist(),
                  'sequence': trials[:,0].astype('int32').tolist(),
                  'starts': trials[:,1].astype('int32').tolist(),
                  'planets': trials[:,2:-1].astype('int32').tolist(),
                  'exp_reward': trials[:,-1].tolist(),
                  'trial_type': trial_type.tolist(),
                  'block': blocks.astype('int32').tolist(),
                  'degradation_blocks': degradation_blocks,
                  'training_blocks': training_blocks,
                  'contingency_degradation': contingency_degradation,
                  'switch_cues': switch_cues,
                  'trials_per_block': trials_per_block,
                  'blocked': blocked,
                  'shuffle': shuffle, 
                  'miniblock_size' : block,
                  'seed':seed,
                  'nr': nr,
                  'state_transition_matrix':[state_transition_matrix.tolist()]*len(context),
                  'reward_contingencies': [[exp_planet_reward.tolist(), exp_planet_reward_switched.tolist()]]*len(context)
                }
        print('created: ', fname)
        with open(fname, "w") as file:
            js.dump(config, file)


def create_config_files(training_blocks,     # how many blocks of training contingency
                       degradation_blocks,   # how many blocks of second degradation contintency
                       extinction_blocks,    # how many blocks where no feedback or reward is given; trials are optimal under the training contingency
                       trials_per_block,     # how many trials per block there are
                       habit_pol = 3,        # the policy which will always be optimal in one of the contexts
                       blocked=False,        # whether trials with different contexts will be presented in non-random, blocked manner ('context1, context1, context1, context2, context2, context2, context1.....)
                       block=None,           # how many trials per context block
                       nr=3,                 # how many rewards are possible 
                       seed=1):              # rng seed

    trials = create_trials_for_both_contingencies(seed=seed)

    degradation = [True]                     # whether there will be a reversal of contingencies
    cue_switch = [False]                     # whether the context cue will be switched
    
    # create a list of all config files to be created
    arrays = [degradation, cue_switch, degradation_blocks, training_blocks, extinction_blocks, trials_per_block,[blocked],[block]]
    lst = [ list(i) for i in product(*arrays)]

    print('Simulation parameters: contingency_degradation, switch_cues, degradation_blocks, training_blocks, extinction_blocks, trials_per_block, context_blocks, block')
    for l in lst:
        print(l)
        create_trials_planning(trials,
                               contingency_degradation=l[0],
                               switch_cues=l[1],
                               degradation_blocks=l[2],
                               training_blocks=l[3],
                               extinction_blocks=l[4],
                               trials_per_block=l[5],
                               habit_pol = habit_pol,
                               blocked = l[6],
                               block=l[7])



# create_state_transition_matrix(6,2,np.array([[1,2,3,4,5,0],[2,3,4,5,0,1]]))
# create_state_transition_matrix(6,2,np.array([[1,2,3,4,5,0],[4,3,4,5,1,1]]))


#%%
############# Config file parameters ###################

conf = ['blocked', 'not_blocked']                # folders where different types of task trials ('blocked' context or 'not blocked') will be saved
data_folder='config'

for con in conf:                                 # create folder is they don't already exist
    path = os.path.join(data_folder, con)
    if not os.path.exists(path):
        os.makedirs(path)


na = 2                                          # how many actions are possible
T = 4                                           # episode length 
matrix = 'new'                                  # refers to whether newer simpler planet transition is used or the old Space Adventure Task one
n_planet_positions = 6                          # how many planets in a constelation
n_planets = 3                                   # how many unique planets
rewards = np.array([-1,0,1])                    # what are the unique rewards
nr = rewards.size                               # how many rewards are possible
n_reward_contingencies = 2                      # how many training contingencies p(r|s)
 
# for some reasion matrix not transposed?
if matrix == "new":
    state_transition_matrix = create_state_transition_matrix(6,2,np.array([[1,2,3,4,5,0],[2,3,4,5,0,1]]))
else:
    state_transition_matrix = create_state_transition_matrix(6,2,np.array([[1,2,3,4,5,0],[4,3,4,5,1,1]]))

policies = sequence_of_length_n(np.arange(2),T-1)
pol_ind = np.arange(policies.shape[0])

if nr == 3:
    planet_reward_probs = np.array([[0.95, 0   , 0   ],
                                    [0.05, 0.95, 0.05],
                                    [0,    0.05, 0.95]])            # p(r|s) before contingency switch
    planet_reward_probs_switched = np.array([[0   , 0    , 0.95],
                                             [0.05, 0.95 , 0.05],
                                             [0.95, 0.05 , 0.0]])   # p(r|s) after contingency switch
elif nr == 2:
    raise('no reward distribution for nr=2 specified! Please provide one.')

exp_planet_reward = planet_reward_probs.T.dot(rewards)                      #p(s|r)p(r)
exp_planet_reward_switched = planet_reward_probs_switched.T.dot(rewards)
contingency_exp_rewards = np.array([exp_planet_reward,exp_planet_reward_switched])


#%% 
###############  Create config files ###############
# thesis agent testing calls
create_config_files([4],[4],[2],[70], blocked=True, block=5, nr=3)
create_config_files([4],[4],[2],[70], blocked=False, nr=3)


# create_config_files([4],[4],[0],[70],blocked=False)
# create_config_files([4],[2],[42],blocked=True, block=3)
# create_config_files([4],[6],[42],blocked=True, block=3)
# create_config_files([6],[6],[42],blocked=True, block=3,nr=2)
# create_config_files([6],[6],[70],blocked=True, block=5,nr=2)
# create_config_files([2],[2],[1],[42], blocked=True, block=3,nr=3)
# create_config_files([1],[1],[1],[28],blocked=True, block=2,nr=3)
# create_config_files([6],[6],[2],[42],blocked=True, block=3,nr=3)
# create_config_files([6],[6],[2],[70],blocked=True, block=5,nr=3)

# real experiment generation calls
# create_config_files([5],[0],[0],[42], blocked=True, block=3,nr=3)
# create_config_files([2],[2],[2],[42], blocked=True, block=3,nr=3)

"""FILE SANITY CHECK"""

#%% can be used to browse the created dataframe
# import json 
# import pandas as pd
# import numpy as np

# fname = '/home/terra/Nextcloud2/projects/BalancingControl/config/not_blocked/planning_config_degradation_1_switch_0_train4_degr4_n70_nr_3.json'
# f = open(fname)
# data = json.load(f)
# df = pd.DataFrame.from_dict(data)

# exp_rewards = df.groupby(['block','context']).mean('exp_reward')['exp_reward']
# print(df.columns)
# print(df.shape)
# print()
# print(exp_rewards)
# # print(df.head(50))
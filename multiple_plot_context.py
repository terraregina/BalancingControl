import sys
import pickle
import os
import json
import jsonpickle as pickle
import jsonpickle.ext.numpy as jsonpickle_numpy
from itertools import product, repeat

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import numpy as np
import torch as ar

import matplotlib.gridspec as gridspec  
import string
import matplotlib.patheffects as pe
from sim_parameters import *

names = ['/home/terra/Nextcloud2/projects/BalancingControl/temp/blocked/multiple_hier_switch0_degr1_p0.8_learn_rew0_q0.85_h1_70_44_decp1_rew0_u1-9-90_3_blocked.json',
         '/home/terra/Nextcloud2/projects/BalancingControl/temp/blocked/multiple_hier_switch0_degr1_p0.8_learn_rew0_q0.85_h100_70_44_decp1_rew0_u1-9-90_3_blocked.json']

# config_name = ['planning_config_degradation_1_switch_0_train4_degr4_n70_nr_3']

dfs = []

for f,fname in enumerate(names):
        # load file
        print(fname)

        jsonpickle_numpy.register_handlers()
        with open(fname, 'r') as infile:
            data = json.load(infile)
        worlds = pickle.decode(data)

        # load config file    
        meta = worlds[-1]
        conf_file = open(os.path.join(os.getcwd(), 'config', fname.split(os.sep)[-2], meta['config_file']))
        config = json.load(conf_file)
        conf_file.close()

        nw = len(worlds)

